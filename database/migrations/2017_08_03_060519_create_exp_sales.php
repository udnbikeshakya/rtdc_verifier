<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exp_sales', function (Blueprint $table) {
            $table->increments('id_sales');
            $table->string('branchcode');
            $table->string('dsecode');
            $table->string('custcode');
            $table->string('invno');
            $table->date('invdate');
            $table->string('skuccode');
            $table->string('skudcode');
            $table->string('skueancode');
            $table->string('batchcode');
            $table->int('qty');
            $table->double('grossamount');
            $table->double('taxamount');
            $table->double('initdisc');
            $table->double('linedisc');
            $table->double('netamount');
            $table->string('invref');
            $table->date('selecteddate');
            $table->int('upc');
            $table->double('retailing');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exp_sales');
    }
}
