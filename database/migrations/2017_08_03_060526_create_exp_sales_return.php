<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpSalesReturn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exp_sales_return', function (Blueprint $table) {
            $table->increments('id_sales_return');
            $table->string('branchcode');
            $table->string('dsecode');
            $table->string('custcode');
            $table->string('srnno');
            $table->string('invno');
            $table->string('srndate');
            $table->string('skuccode');
            $table->string('skudcode');
            $table->string('skueancode');
            $table->string('batchcode');
            $table->int('qty');
            $table->date('selecteddate');
            $table->double('grossinvamt');
            $table->double('netinvamt');
            $table->double('linedisc');
            $table->double('initdisc');
            $table->string('taxes');
            $table->int('upc');
            $table->double('retailing');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('exp_sales_return');

    }
}
