<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpRetailer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exp_retailer', function (Blueprint $table) {
            $table->increments('id_retailer');
            $table->string('branchcode');
            $table->string('custcode');
            $table->string('custname');
            $table->string('channel');
            $table->string('dse');
            $table->string('custtype');
            $table->string('town');
            $table->string('townclass');
            $table->string('area');
            $table->string('coveragests');
            $table->date('date');
            $table->date('selecteddate');
            $table->string('custclass');
            $table->string('status');
            $table->string('mracccde');
            $table->string('dsecde');
            $table->string('latitude');
            $table->string('longitude');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('exp_retailer');

    }
}
