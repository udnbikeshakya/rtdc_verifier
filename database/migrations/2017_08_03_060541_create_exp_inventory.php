<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpInventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exp_inventory', function (Blueprint $table) {
            $table->increments('id_inventory');
            $table->string('branchcode');
            $table->string('skuccode');
            $table->int('qty');
            $table->double('giv');
            $table->double('niv');
            $table->int('prdtype');
            $table->string('date');
            $table->string('selecteddate');
            $table->string('upc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('exp_inventory');

    }
}
