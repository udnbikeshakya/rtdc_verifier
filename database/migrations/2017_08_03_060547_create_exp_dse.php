<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpDse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exp_dse', function (Blueprint $table) {
            $table->increments('id_dse');
            $table->string('branchcode');
            $table->string('dsecode');
            $table->string('dsename');
            $table->string('stlcode');
            $table->string('stlname');
            $table->string('becode');
            $table->string('bename');
            $table->string('sectorname');
            $table->date('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('exp_dse');

    }
}
